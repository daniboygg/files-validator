from os import path

from setuptools import setup, find_packages

this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='django-files-validator',
    version='0.1.4',
    description='A Django app to validate files.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/danigayosog/files-validator/',
    author='Daniel Gayoso González',
    author_email='danigayosog@gmail.com',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
    ],
    packages=find_packages(include=['files_validator', 'files_validator.*']),
    include_package_data=True,
    install_requires=[
        "Django>=2.2.7",
    ],
)
