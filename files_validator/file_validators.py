import os
from abc import ABC, abstractmethod
from importlib import import_module
from typing import List, Type

from django.conf import settings
from django.core.files.uploadedfile import InMemoryUploadedFile


class FileValidator(ABC):
    valid_extensions = None
    validator_name = None

    def validate_files(self, files: List[InMemoryUploadedFile]) -> List[str]:
        errors = []
        for file_name in files:
            try:
                self.validate_file_name(file_name.name)
                errors += self.validate_file(file_name)
            except InvalidFileError as e:
                errors.append(str(e))
        return errors

    @abstractmethod
    def validate_file(self, filename: InMemoryUploadedFile) -> List[str]:
        pass

    def validate_files_names(self, files: List[str]):
        for file_name in files:
            if not self.validate_file_name(file_name):
                return False
        return True

    def validate_file_name(self, filename: str):
        _, extension = os.path.splitext(filename)
        valid_extensions = self.get_valid_extensions()
        if extension.lower() not in valid_extensions:
            raise InvalidFileError('{} has not valid extensions. '
                                   'Posible values: {}'.format(filename, ','.join(valid_extensions)))

    def get_valid_extensions(self) -> List[str]:
        assert self.valid_extensions is not None, \
            'You must provide valid_extensions as class attribute or override get_valid_extensions'
        return self.valid_extensions

    @classmethod
    def get_validator_name(cls) -> str:
        assert cls.validator_name is not None, 'You must provide a class attribute validator_name'
        return cls.validator_name


class InvalidFileError(Exception):
    pass


class ValidatorFactory(ABC):

    @classmethod
    def get_instance(cls, validator_name: str) -> 'FileValidator':
        validator_classes = cls.get_validators()
        for validator in validator_classes:
            if validator.get_validator_name() == validator_name:
                return validator()

        raise ValueError('Validator {} is not a valid validator.'.format(validator_name))

    @classmethod
    def get_validator_names(cls) -> List[str]:
        return [validator.get_validator_name() for validator in cls.get_validators()]

    @classmethod
    def get_validators(cls) -> List[Type[FileValidator]]:
        validators = []
        setting_validators = settings.FILE_VALIDATORS
        assert setting_validators is not None, 'You must provide settings.FILE_VALIDATORS'

        for validator in setting_validators:
            validator_package_name = '.'.join(validator.split('.')[:-1])
            validator_class_name = validator.split('.')[-1]

            validator_class = getattr(import_module(validator_package_name), validator_class_name)
            validators.append(validator_class)
        return validators
