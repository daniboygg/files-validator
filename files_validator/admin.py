from django.contrib import admin

# Register your models here.
from files_validator import models


@admin.register(models.FileHandler)
class FileHandlerAdmin(admin.ModelAdmin):
    pass
