import json
import os
import shutil
from typing import List

from django.conf import settings
from django.http import JsonResponse
from django.views import generic

from files_validator import models
from files_validator.file_validators import ValidatorFactory, InvalidFileError
from files_validator.models import path_from_path_with_pound


class FileList(generic.ListView):
    model = models.FileHandler
    template_name = 'files_validator/filehandler_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['file_list'] = [{
            'name': file.file.name,
            'id': file.id,
        } for file in self.object_list]
        return context


class APIFileList(FileList):
    model = models.FileHandler

    def render_to_response(self, context, **response_kwargs):
        return JsonResponse(data={
            'files': context['file_list']
        })


class APIValidators(generic.View):

    def get(self, request, *args, **kwargs):
        return JsonResponse(data={
            'validators': ValidatorFactory.get_validator_names()
        })


class APIUploadFiles(generic.View):

    def post(self, request, *args, **kwargs):
        post_validator = request.POST.get('validator', '')
        if post_validator == '':
            return self.create_json_response(['You must provide a valid validator'])

        validator = ValidatorFactory.get_instance(post_validator)
        files = self.request.FILES.getlist('files')
        errors = []
        files_to_process = []
        for file_to_store in files:
            real_path = path_from_path_with_pound(file_to_store.name)
            try:
                validator.validate_file_name(real_path)
                old_file = models.FileHandler.objects.get(file=real_path)
                old_file.file = file_to_store
                old_file.save()
                files_to_process += file_to_store
            except InvalidFileError as e:
                errors.append(str(e))
            except models.FileHandler.DoesNotExist:
                models.FileHandler.objects.create(file=file_to_store)

        errors += validator.validate_files(files_to_process)

        if not errors:
            errors.append('Success!')

        return self.create_json_response(errors)

    @staticmethod
    def create_json_response(errors: List[str]) -> JsonResponse:
        return JsonResponse(data={
            'errors': errors
        })


class APIDeleteFileList(generic.View):

    def post(self, request, *args, **kwargs):
        json_data = json.loads(request.body)
        files_to_delete = models.FileHandler.objects.all()
        remove_folders = False
        try:
            files_to_delete = files_to_delete.filter(id=json_data['id'])
        except KeyError:
            remove_folders = True
        for file_to_delete in files_to_delete:
            file_to_delete.file.delete()
            file_to_delete.delete()

        if remove_folders:
            for dirpath, dirname, filename in os.walk(settings.MEDIA_ROOT):
                for folder in dirname:
                    if folder != 'to_moodle' and folder != 'imagenes':
                        shutil.rmtree(os.path.join(dirpath, folder))

        return JsonResponse(data={})
